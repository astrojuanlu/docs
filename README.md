# Documentos esLibre

A través de este proyecto se gestiona la documentación necesaría y útil para
la organización de las conferencias esLibre, como pueden serÑ

* Gobernanza
* Descripción de qué es (y qué no es) esLibre
* Código de conducta
* Infrastructuras para las personas de organización (otros repositorios, canales de comunicación, etc.)
* 
# Cómo contribuir

Si crees que falta algún documento o quieres agregar alguno específico, puedes
solicitarlo abriendo un issue nuevo.

Si crees que hay algún error en los documentos existentes, puedes indicarlo
abriendo un issue.